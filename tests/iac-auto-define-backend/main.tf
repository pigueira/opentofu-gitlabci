resource "local_file" "foo" {
  content  = "foo!"
  filename = "${path.module}/foo.bar"
}

locals {
  ts = plantimestamp()
}

// NOTE: always force a change.
resource "null_resource" "this" {
  triggers = {
    timestamp = local.ts
  }
}

output "this_always_changes" {
  value = local.ts
}
