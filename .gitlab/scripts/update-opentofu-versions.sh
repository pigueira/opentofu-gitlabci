#!/usr/bin/env sh

set -e

script_dir="$(dirname "$0")"
project_dir="$script_dir/../.."

echo "Updating template files ..."

templates="templates/*.yml"
templates_exclude="templates/delete-state.yml templates/module-release.yml"

for relative_template_file in $templates; do
  if echo "$templates_exclude" | grep -q "$relative_template_file"; then continue; fi
  template_file="$project_dir/$relative_template_file"
  tmp_template_file1=$(mktemp)
  tmp_template_file2=$(mktemp)
  echo "Updating $template_file ... "
  yq eval-all '
    select(fileIndex == 0 and document_index == 0).spec.inputs.opentofu_version.default
     = select(fileIndex == 1).".data".latest_version
     | select(fileIndex == 0)
    ' "$template_file" "$project_dir/opentofu_versions.yaml" > "$tmp_template_file1"
  yq eval-all '
    select(fileIndex == 0 and document_index == 0).spec.inputs.opentofu_version.options
     = (select(fileIndex == 1).".data".hack_templates_supported_versions
       | explode(.)
       | flatten
     ) | select(fileIndex == 0)
  ' "$tmp_template_file1" "$project_dir/opentofu_versions.yaml" > "$tmp_template_file2"
  diff -Bw "$template_file" "$tmp_template_file2" | patch "$template_file" -
done

renovate_config="default.json"
echo "Updating Renovate config $renovate_config ... "
yq eval-all -p yaml -I4 -o json -i '
    select(fileIndex == 0 and documentIndex == 0).packageRules[0].allowedVersions
     = "<="
     + select(fileIndex == 1 and documentIndex == 0).".data".latest_version
     | select(fileIndex == 0)
  ' "$renovate_config" "$project_dir/opentofu_versions.yaml"
